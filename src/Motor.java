
public class Motor implements Apagable {

	private static int CONTADOR_MOTORES = 0;

	private int id;
	
	private boolean encendido;
	
	public Motor() {
		id = ++CONTADOR_MOTORES;
		encendido = false;
	}
	
	@Override
	public boolean encender() {
		if (encendido) {
			System.out.printf(
					"Soy la motor %d y estoy encendido.\n", id);
			return false;
		} else {
			encendido = true;
			System.out.printf("Soy la motor %d y me encendi.\n", id);
			return true;
		}
	}

	@Override
	public boolean apagar() {
		if (!encendido) {
			System.out.printf(
					"Soy la motor %d y estoy apagada.\n", id);
			return false;
		} else {
			encendido = false;
			System.out.printf("Soy la motor %d y me apague.\n", id);
			return true;
		}
	}

}
