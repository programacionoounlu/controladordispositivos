
public class EquipoAudio implements Apagable {

	private static int CONTADOR_MOTORES = 0;

	private int id;
	
	private boolean encendido;
	
	public EquipoAudio() {
		id = ++CONTADOR_MOTORES;
		encendido = false;
	}
	

	public boolean switchOn() {
		if (encendido) {
			System.out.printf(
					"Soy la motor %d y estoy encendido.\n", id);
			return false;
		} else {
			encendido = true;
			System.out.printf("Soy la motor %d y me encendi.\n", id);
			return true;
		}
	}


	public boolean switchOff() {
		if (!encendido) {
			System.out.printf(
					"Soy la motor %d y estoy apagada.\n", id);
			return false;
		} else {
			encendido = false;
			System.out.printf("Soy la motor %d y me apague.\n", id);
			return true;
		}
	}


	@Override
	public boolean encender() {
		return switchOn();
	}


	@Override
	public boolean apagar() {
		// TODO Auto-generated method stub
		return switchOff();
	}

}
