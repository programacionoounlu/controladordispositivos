import java.util.ArrayList;

public class Cliente {
	private ArrayList<Apagable> apagables;
	
	public Cliente() {
		apagables = new ArrayList<>();
	}
	
	public void agregarApagable(Apagable apagable) {
		apagables.add(apagable);
	}
	
	public void encender(int i) {
		apagables.get(i).encender();
	}
	
	public void apagar(int i) {
		apagables.get(i).apagar();
	}
 }
