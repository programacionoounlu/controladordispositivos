
public class Lampara implements Apagable {
	private static int CONTADOR_LAMPARAS = 0;

	private int id;
	
	private boolean encendido;
	
	public Lampara() {
		id = ++CONTADOR_LAMPARAS;
		encendido = false;
	}
	
	@Override
	public boolean encender() {
		if (encendido) {
			System.out.printf(
					"Soy la lampara %d y estoy encendido.\n", id);
			return false;
		} else {
			encendido = true;
			System.out.printf("Soy la lampara %d y me encendi.\n", id);
			return true;
		}
	}

	@Override
	public boolean apagar() {
		if (!encendido) {
			System.out.printf(
					"Soy la lampara %d y estoy apagada.\n", id);
			return false;
		} else {
			encendido = false;
			System.out.printf("Soy la lampara %d y me apague.\n", id);
			return true;
		}
	}

}
