
public interface Apagable {
	boolean encender();
	boolean apagar();
}
